import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';


import { StoreModule } from '@ngrx/store';
import { reducer } from './reducers/tutorial.reducer';
import { ReadComponent } from './read/read.component';
import { CreateComponent } from './create/create.component';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router'
import { freeApiService } from './services/freeapi.service';
import { EmployeeComponent } from './employee/employee.component';
import { NumberValidateDirective } from './number-validate.directive';
import { NumberValidComponent } from './number-valid/number-valid.component';
import { FormsModule } from '@angular/forms';
import { EmployeeFormComponent } from './employee-form/employee-form.component';
import { HomeComponent } from './home/home.component';
import { HttpModule } from '@angular/http';
import { ProductComponent } from './product/product.component';
import { UpdateProductComponent } from './update-product/update-product.component';
<<<<<<< HEAD
import { ParentComponentComponent } from './parent-component/parent-component.component';
import { ChildComponentComponent } from './child-component/child-component.component';
=======
>>>>>>> Inital Commit

@NgModule({
  declarations: [
    AppComponent,
    ReadComponent,
    CreateComponent,
    EmployeeComponent,
    NumberValidateDirective,
    NumberValidComponent,
    EmployeeFormComponent,
    HomeComponent,
    ProductComponent,
<<<<<<< HEAD
    UpdateProductComponent,
    ParentComponentComponent,
    ChildComponentComponent
=======
    UpdateProductComponent
>>>>>>> Inital Commit
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    StoreModule.forRoot({
      tutorial: reducer
    }),
    StoreDevtoolsModule.instrument({
      maxAge: 3
    }),
<<<<<<< HEAD
    // RouterModule.forRoot([
    //   // {path: "", component: HomeComponent},
    //   // {path: "product", component: ProductComponent},
    //   // {path: "updateProduct/:id", component: UpdateProductComponent} 
    // ])
=======
    RouterModule.forRoot([
      {path: "", component: HomeComponent},
      {path: "product", component: ProductComponent},
      {path: "updateProduct/:id", component: UpdateProductComponent} 
    ])
>>>>>>> Inital Commit
  ],
  providers: [freeApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
