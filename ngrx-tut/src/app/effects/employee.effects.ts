import { Injectable } from '@angular/core';

import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';

import { Observable, of } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';

import { freeApiService } from '../services/freeapi.service';
import * as tutorialActions from '../actions/tutorial.actions';
import { Tutorial } from '../modals/tutorial.modal';

@Injectable()
export class EmployeeEffects {

    constructor(
        private actions$: Actions,
        private freeApiService: freeApiService
    ) {}

    @Effect()
    addTutorial$: Observable<Action> = this.actions$.pipe(
        ofType<tutorialActions.AddTutorial>(
            tutorialActions
        )
    )
}