import { Component } from '@angular/core';

@Component({
  selector: 'app-number-valid',
  templateUrl: './number-valid.component.html',
  styleUrls: ['./number-valid.component.css']
})
export class NumberValidComponent {
  
  value='';
  counter = 0;

  onChange(event) {
    this.counter = this.counter + 1;  
  } 

}
