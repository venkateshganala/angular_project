import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumberValidComponent } from './number-valid.component';

describe('NumberValidComponent', () => {
  let component: NumberValidComponent;
  let fixture: ComponentFixture<NumberValidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumberValidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumberValidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
