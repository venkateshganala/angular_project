import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private http: Http) { }
  id:number;
  private headers = new Headers({ 'Content-Type': 'application/json'});

  products = [];
  fetchData = function() {
<<<<<<< HEAD
    this.http.get("http://localhost:5555/products/").subscribe(
=======
    this.http.get("http://localhost:5555/products").subscribe(
>>>>>>> Inital Commit
      (res: Response) => {
        this.products = res.json();
      }
    )
  }

  deleteProduct = function(id) {
    if(confirm("Are you sure?")) {
<<<<<<< HEAD
      const url = `${"http://localhost:5555/products/"}/${id}`;
=======
      const url = `${"http://localhost:5555/products"}/${id}`;
>>>>>>> Inital Commit
      return this.http.delete(url, {headers: this.headers}).toPromise()
        .then(() => {
            this.fetchData();
        })
    }
  }

  ngOnInit() {
    this.fetchData();
  }

}
